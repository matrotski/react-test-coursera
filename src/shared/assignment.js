export const CONTACTS_TITLE = 'Contact Information';
export const CONTACTS_ITEMS = ' 121, Clear Water Bay Road \n\n Clear Water Bay, Kowloon \n\n HONG KONG \n\n Tel: +852 1234 5678 \n\n Fax: +852 8765 4321 \n\n Email:confusion@food.net';
export const HISTORY_TITLE = 'Our History';
export const HISTORY_DESCRIPTION = "Started in 2010, Ristorante con Fusion quickly established itself as a culinary icon par excellence in Hong Kong. With its unique brand of world fusion cuisine that can be found nowhere else, it enjoys patronage from the A-list clientele in Hong Kong.  Featuring four of the best three-star Michelin chefs in the world, you never know what will arrive on your plate the next time you visit us. \n\nThe restaurant traces its humble beginnings to The Frying Pan, a successful chain started by our CEO, Mr. Peter Pan, that featured for the first time the world's best cuisines in a pan."
export const ABOUT_TITLE = 'Corporate Leadership';

