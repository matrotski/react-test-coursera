import React, { Component } from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';
import { Icon, Input, CheckBox, Button } from 'react-native-elements';
import { SecureStore, Permissions, ImagePicker, ImageManipulator } from 'expo';
import { createBottomTabNavigator } from 'react-navigation';
import { baseUrl } from '../shared/baseUrl';
import { ScrollView } from 'react-native-gesture-handler';

debugger
class LoginTab extends Component {

    constructor(props)
    {
        super(props);
        this.state = {
            username: '',
            password: '',
            remember: false
        }
    }

    componentDidMount() {
        SecureStore.getItemAsync('userInfo')
        .then((userDate) => {
            let userInfo = JSON.parse(userDate);
            if (userInfo)
            {
                this.setState({userName: userInfo.userName});
                this.setState({password: userInfo.password});
                this.setState({remember: true});
            }
        })
    }

    static navigationOptions = {
        title: 'Login',
        tabBarIcon: ({ tintColor }) => (
            <Icon
                name='sign-in'
                type='font-awesome'
                size={24}
                iconStyle={{ color: tintColor }}
            />
        )
    };

    handleLogin() {
        console.log(JSON.stringify(this.state));
        if (this.state.remember) {
            SecureStore.setItemAsync(
                'userInfo',
                JSON.stringify({
                    userName: this.state.userName,
                    password: this.state.password
                })
            )
            .catch((error) => console.log('Could not save user or password', error));
        }
        else {
            SecureStore.deleteItemAsync('userInfo')
            .catch((error) => console.log('Could not delete user info', error));
        }
    }

    render() {
        return(
            <ScrollView>
            <View style={styles.container}>
                <Input 
                    placeholder="Username"
                    onChangeText={(userName) => this.setState({userName})}
                    value={this.state.userName}
                    containerStyle={styles.FormInput}
                />
                <Input 
                    placeholder="Password"
                    onChangeText={(password) => this.setState({password})}
                    value={this.state.password}
                    containerStyle={styles.FormInput}
                /> 
                <CheckBox
                    title="Remember Me"
                    center
                    checked={this.state.remember}
                    onPress={() => this.setState({remember: ! this.state.remember})}
                    containerStyle={styles.formCheckbox}
                />
                <View style={styles.formButton}>
                    <Button
                        onPress={() => this.handleLogin()}
                        text='Login'
                        icon={
                            <Icon
                                name='sign-in'
                                type='font-awesome'
                                size={24}
                                color='white'
                            />
                        }
                        buttonStyle={{
                            backgroundColor: '#512DA8'
                        }}
                    />
                </View>
                <View style={styles.formButton}>
                    <Button
                        onPress={() => this.props.navigation.navigate('Register')}
                        text='Register'
                        clear
                        icon={
                            <Icon
                                name='user-plus'
                                type='font-awesome'
                                size={24}
                                color='blue'
                            />
                        }
                        textStyle={{
                            color: 'blue'
                        }}
                    />
                </View>
            </View>
            </ScrollView>
        );
    }
}

debugger
class RegisterTab extends Component {

    constructor(props)
    {
        super(props)
        this.state = {
            username: '',
            password: '',
            firstname: '',
            lastname: '',
            email: '',
            remember: false,
            imageUrl: baseUrl + 'images/logo.png'
        }
    }

    getImageFromGallery = async () => {
        const galleryPermission = await Permissions.askAsync(Permissions.CAMERA_ROLL);
        if (galleryPermission.status === 'granted')
        {
            let galleryImage = await ImagePicker.launchImageLibraryAsync({
                allowsEditing: true,
                aspect: [4, 3] 
            });
            if (!capturedImage.cancelled) {
                this.processImage( capturedImage.uri )
            }
        }
    }

    getImageFromCamera = async () => {
        const cameraPermission = await Permissions.askAsync(Permissions.CAMERA);
        const cameraRollPermission = await Permissions.askAsync(Permissions.CAMERA_ROLL);

        if (cameraPermission.status === 'granted' && cameraRollPermission.status === 'granted')
        {
            let capturedImage = await ImagePicker.launchCameraAsync({
                allowsEditing: true,
                aspect: [4, 3]
            });
            if (!capturedImage.cancelled) {
                this.processImage( capturedImage.uri )
            }
        }
    }

    processImage = async (imageUri) => {
        let processedImage = await ImageManipulator.manipulateAsync(
            imageUri,
            [
                { resize: { width: 400 }}
            ],
            { format: 'png' }
        );
        this.setState({ imageUrl: processedImage.uri })
    } 

    handleRegister()
    {
        console.log(JSON.stringify(this.state));
        if (this.state.remember) {
            SecureStore.setItemAsync(
                'userInfo',
                JSON.stringify({
                    userName: this.state.userName,
                    password: this.state.password
                })
            )
            .catch((error) => console.log('Could not save user or password', error));
        }
        else {
            SecureStore.deleteItemAsync('userInfo')
            .catch((error) => console.log('Could not delete user info', error));
        }    
    }

    static navigationOptions = {
        title: 'Register',
        tabBarIcon: ({ tintColor }) => (
            <Icon
                name='user-plus'
                type='font-awesome'
                size={24}
                iconStyle={{ color: tintColor }}
            />
        )
    };

    render() {
        return(
            <ScrollView>
            <View style={styles.container}>
                <View style={styles.container}>
                    <Image
                        source={{
                            uri: this.state.imageUrl
                        }}
                        loadingIndicatorSource={require('./images/logo.png')}
                        style={styles.image}
                    />
                    <View style={styles.imageButtons}>
                        <Button
                            text='Camera'
                            onPress={this.getImageFromCamera}
                        />
                        <Button
                            text='Gallery'
                            onPress={this.getImageFromGallery}
                        />
                    </View>
                </View>
                <Input 
                    placeholder="Username"
                    onChangeText={(username) => this.setState({username})}
                    value={this.state.username}
                    containerStyle={styles.FormInput}
                />
                <Input 
                    placeholder="Password"
                    onChangeText={(password) => this.setState({password})}
                    value={this.state.password}
                    containerStyle={styles.FormInput}
                />
                <Input 
                    placeholder="Firstname"
                    onChangeText={(userName) => this.setState({userName})}
                    value={this.state.userName}
                    containerStyle={styles.FormInput}
                />
                <CheckBox
                    title="Remember Me"
                    center
                    checked={this.state.remember}
                    onPress={() => this.setState({remember: ! this.state.remember})}
                    containerStyle={styles.formCheckbox}
                />
                <View style={styles.formButton}>
                    <Button
                        onPress={() => this.handleRegister()}
                        text='Register'
                        color="#512DA8"
                    />
                </View>
            </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        margin: 20      
    },
    FormInput: {
        margin: 40
    },
    formCheckbox: {
        margin: 0,
        backgroundColor: null
    },
    formButton: {
        margin: 60
    },
    imageContainer: {
        flex: 1,
        flexDirection: 'row',
        margin: 20
    },
    image: {
      margin: 10,
      width: 80,
      height: 60
    },
    imageButtons: {
        justifyContent: 'space-around',
        flex: 1,
        flexDirection: 'row'

    }
});

const Login = createBottomTabNavigator({
        Login: LoginTab,
        Register: RegisterTab
    },{
        tabBarOptions:{
            activeBackgroundColor: '#9575CD',
            inactiveBackgroundColor: '#D1C4E9',
            activeTintColor: '#ffffff',
            inactiveTintColor: 'gray'
        }
})

export default Login;
