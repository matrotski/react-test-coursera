import React, { Component } from 'react';
//import { DISHES }  from '../shared/dishes';
import { Text, View, ScrollView, FlatList, Modal, Alert, PanResponder, Share } from 'react-native';
import { Button, Input, Card, Icon } from 'react-native-elements';
//import { COMMENTS } from '../shared/comments';
import { connect } from 'react-redux';
import { baseUrl } from '../shared/baseUrl';
import { postFavorite, postComment } from '../redux/ActionCreators';
import { Rating } from 'react-native-ratings'

import * as Animatable from 'react-native-animatable';

function RenderComments(props) {

    const comments = props.comments;

    var view = null;

    var handleViewRef = ref => view = ref;

    const recognizeDrag = ({ moveX, moveY, dx, dy }) =>
    {
        if (dx > 200)
            return true;
        else
            return false;
    }

    const panResponder = PanResponder.create({
        onStartShouldSetPanResponder: (e, gestureState) =>
        {
            return true;
        },
        onPanResponderGrant: () => {
             view.rubberBand(1000)
             .then(endState => console.log(endState.finished ? 'finished' : 'cancelled'))
        },
        onPanResponderEnd: (e, gestureState) => {
            if (recognizeDrag(gestureState))
                props.onEdit(); 
            return true;
        }
    });

    const renderCommentItem = ({item, index}) =>
    {
        return (
            <Animatable.View animation="fadeInUp" duration={2000} delay={1000}>
                <View key={index} style={{margin: 10}}>
                    <Text style={{fontSize: 14}}>{item.comment}</Text>
                    <Text style={{fontSize: 12}}>{item.rating} Stars</Text>
                    <Rating
                        style={{flex: 1, alignItems: 'flex-start', paddingVertical: 5}}
                        imageSize={10}
                        fractions={2}
                        startingValue={item.rating}
                    />
                    <Text style={{fontSize: 12}}>{'-- ' + item.author + ', ' + item.date}</Text>
                </View>
            </Animatable.View>
        );
    };

    return (
        <Animatable.View animation="fadeInUp" duration={2000} delay={1000}
            ref={handleViewRef}
            {...panResponder.panHandlers}>
            <Card title='Comments'>
                <FlatList 
                    data={comments}
                    renderItem={renderCommentItem}
                    keyExtractor={item => item.id.toString()}
                />
            </Card>
        </Animatable.View>
    );
}

function RenderDish(props) {
    const dish = props.dish;
    
    var view = null;

    var handleViewRef = ref => view = ref;

    const recognizeDrag = ({ moveX, moveY, dx, dy }) => {
        if (dx < -200)
            return true;
        else
            return false;
    };

    const panResponder = PanResponder.create({
        onStartShouldSetPanResponder: (e, gestureState) => {
            return true;
        },
        onPanResponderGrant: () => {
            view.rubberBand(1000)
            .then(endState => console.log(endState.finished ? 'finished' : 'cancelled')) 
        },
        onPanResponderEnd: (e, gestureState) => {
            if (recognizeDrag(gestureState))
                Alert.alert(
                    'Add to Favorites?',
                    'Are you sure you wish to add ' + dish.name + ' to your favorites?',
                    [
                        {
                            text: 'Cancel',
                            onPress: () => console.log('Cancel pressed'),
                            style: 'cancel'
                        },
                        {
                            text: 'OK',
                            onPress: () => {

                            }
                        }
                    ],
                    { cancelable: false }
                )
            return true;
        }
    });

    const shareDish = (title, message, url) => {
        Share.share({
            title: title,
            message: title + ': ' + message + ' ' + url,
            url: url
        }, {
            dialogTitle: 'Share ' + title 
        });
    }

    if (dish != null) {
        return(
            <Animatable.View animation="fadeInDown" duration={2000} delay={1000}
            ref = {handleViewRef}
                {...panResponder.panHandlers}>
                <Card
                    featuredTitle={dish.name}
                    image={{source: {uri: baseUrl + dish.image}}}>
                    <Text style={{margin: 10}}>
                        {dish.description}
                    </Text>
                    <View style={{flex: 1, flexDirection: 'row', justifyContent: 'center'}}>
                        <Icon 
                            raised
                            reverse
                            name={ props.favorite ? 'heart' : 'heart-o'}
                            type='font-awesome'
                            color='#f50'
                            onPress={() => props.favorite ? console.log('Already favorite') : props.onPress()}
                        />
                        <Icon
                            raised
                            reverse
                            name={'edit'}
                            type='material-icon'
                            color='#512da7'
                            onPress={ () => { props.onEdit(); } }
                        /> 
                        <Icon 
                            raised
                            reverse
                            name='share'
                            type='font-awesome'
                            color='#51D2A8'
                            onPress={() => shareDish(dish.name, dish.description, baseUrl + dish.image)}
                        />
                    </View> 
                </Card>
            </Animatable.View>
        );
    }
    else {
        return(<View></View>);
    }
}

class Dishdetail extends Component
{
    constructor(props)
    {
        super(props);
        this.state = {
            favorites: [],
            showModal: false,
        };
    }

    static navigationOption = {
        title: 'Dish Details'
    }

    markFavorite(dishId)
    {
        this.props.postFavorite(dishId);
    }

    toggleModal() {
        this.setState({showModal: !this.state.showModal});
    }

    handleComment(comment) {
        this.props.postComment(comment);
        this.toggleModal();
    }

    render()
    {
        const dishId = this.props.navigation.getParam('dishId','');
        var comment = {};
        return(
            <View>
                <Modal animationType={"slide"} transparent={false} 
                    visible={this.state.showModal}
                    onDismiss={ () => {this.toggleModal();} }
                    onRequestClose ={ () => this.toggleModal() }> 
                    <View style={{ flex: 1, flexDirection: 'column', paddingHorizontal: 20 }}>
                        <Rating
                            showRating
                            fractions={2}
                            onFinishRating={(rating) => {
                                comment.rating = rating;
                            }}
                        />
                        <Input
                            placeholder='Author'
                            leftIcon={{ type: 'font-awesome', name: 'user-o' }}
                            onChangeText={(text) => {
                                comment.author = text;
                            }}
                        />
                        <Input 
                            placeholder='Comment' 
                            leftIcon={{ type: 'font-awesome', name: 'comment-o' }}
                            onChangeText={(text) => {
                                comment.comment = text;
                            }}
                        />
                        <Button
                            title='Submit'
                            buttonStyle={{ 
                                marginTop: 30,
                                backgroundColor: '#512da7',
                                shadowColor: '#000',
                                shadowOffset: { width: 2, height: 2 },
                                shadowOpacity: 0.8,
                                shadowRadius: 2,
                                elevation: 3
                            }}
                            onPress={ () => { 
                                if (comment)
                                { 
                                    comment.id = this.props.comments.comments.length
                                    comment.dishId = dishId;
                                    var date = new Date(Date.now());
                                    comment.date = date.toISOString(); 
                                    this.handleComment(comment);
                                }
                            }} 
                        />
                        <Button
                            title='Close'
                            buttonStyle={{ 
                                marginTop: 30,
                                backgroundColor: '#808080',
                                shadowColor: '#000',
                                shadowOffset: { width: 2, height: 2 },
                                shadowOpacity: 0.8,
                                shadowRadius: 2,
                                elevation: 3
                            }}
                            onPress={ () => { this.toggleModal(); }} 
                        />
                    </View>
                </Modal>
                <ScrollView>
                    <RenderDish dish={this.props.dishes.dishes[+dishId]}  
                        favorite={this.props.favorites.some(el => el === dishId)}
                        onPress={() => this.markFavorite(dishId)}
                        onEdit={() => { this.toggleModal(); }}
                    />
                    <RenderComments comments={this.props.comments.comments.filter((comment) => comment.dishId === dishId)}
                                    onEdit={() => { this.toggleModal(); }}
                    />
                </ScrollView>
            </View>
        );  
    }
} 

const mapStateToProps = state => {
    return {
        dishes: state.dishes,
        comments: state.comments,
        favorites: state.favorites,
    }
}

const mapDispatchToProps = dispatch => ({
    postFavorite: (dishId) => dispatch(postFavorite(dishId)),
    postComment: (comment) => dispatch(postComment(comment))
})

export default connect (mapStateToProps, mapDispatchToProps)(Dishdetail);