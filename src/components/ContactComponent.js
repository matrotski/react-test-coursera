import React, { Component } from 'react';
import { Text } from 'react-native';
import { Card, Button, Icon } from 'react-native-elements';
import * as assignments from '../shared/assignment';
import * as Animatable from 'react-native-animatable';
import { MailComposer } from 'expo';

class ContactUs extends Component
{
    constructor(props)
    {
        super(props);
    }

    sendMail() {
        MailComposer.composeAsync({
                recipients: ['mailto:confusion@food.net'],
                subject: 'Enquiry',
                body: 'To whom it may concern:'
        });
    }

    render()
    {
        return(
            <Animatable.View animation = "fadeInDown" duration={2000} delay={1000}>
                <Card
                    title = { assignments.CONTACTS_TITLE }>
                    <Text>
                        { assignments.CONTACTS_ITEMS }
                    </Text>
                    <Button
                        title='Send Email'
                        buttonStyle={{ backgroundColor: '#512DA8'}}
                        icon={<Icon name='envelope-o' type='font-awesome' color='white' />}
                        onPress={this.sendMail}
                    />
                </Card>
            </Animatable.View>
        );
    };
}

export default ContactUs;