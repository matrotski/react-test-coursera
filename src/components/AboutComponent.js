import React, { Component } from 'react';
import { FlatList, View, Text, Image } from 'react-native';
import { Card, ListItem } from 'react-native-elements';
import { LEADERS } from '../shared/leaders';
import * as assignments from '../shared/assignment';
import { ScrollView } from 'react-native-gesture-handler';

import { connect } from 'react-redux';
import { baseUrl } from '../shared/baseUrl';

import { Loading } from './LoadingComponent';

import * as Animatable from 'react-native-animatable';

class History extends Component
{
    render()
    {
        return(
            <Card
                title = { assignments.HISTORY_TITLE }>
                <Text style = { { fontSize: 12 } }>
                    { assignments.HISTORY_DESCRIPTION }
                </Text>
            </Card>
        );
    }
}

class AboutUs extends Component
{
    render()
    {
        var self = this;
        self.renderSeparator= () => {
            return (
                <View
                    style={{
                        height: 0
                    }}
                />
            );
        } 
        const renderLeader = ({ item, index }) => {
            return (
                <ListItem
                    style = { { display: 'none' } }
                    key={ index }
                    title={ item.name }
                    roundAvatar
                    bottomDividerStyle = { {display: 'none'} }
                    subtitle={ item.description }
                    subtitleStyle={ {fontStyle: "normal", fontSize: 10} }
                    subtitleNumberOfLines = {0}   
                    avatar={ <Image source={ {source: {uri: baseUrl + item.image}} } /> }
                />
            );
        }

        if (this.props.leaders.isLoading){
            return (
                <ScrollView>
                        <History />
                        <Card 
                            title='Corporate Leadership'>
                            <Loading />
                        </Card>
                </ScrollView>
            );
        }
        else if (this.props.leaders.errMess)
        {
            return (
                <ScrollView>
                    <Animatable.View animation = "fadeInDown" duration={2000} delay={1000}>
                        <History />
                        <Card 
                            title='Corporate Leadership'>
                            <Text>{this.props.leaders.errMess}</Text>
                        </Card>
                    </Animatable.View>
                </ScrollView>
            );
        }
        else
        {
            return(
                <View>
                    <ScrollView>
                        <Animatable.View animation = "fadeInDown" duration={2000} delay={1000}>
                            <View style = { { paddingBottom: 20 } }>
                                <History />
                                <Card
                                    title = { assignments.ABOUT_TITLE }>
                                    <FlatList 
                                        data={ self.props.leaders.leaders }
                                        renderItem={ renderLeader }
                                        keyExtractor={ item => item.id.toString() }
                                        ItemSeparatorComponent={self.renderSeparator}
                                    />
                                </Card>
                            </View>
                        </Animatable.View>
                    </ScrollView>
                </View>
            );
        }
    };
}

const mapStateToProps = state => {
    return {
        leaders: state.leaders
    }
}

export default connect(mapStateToProps)(AboutUs);